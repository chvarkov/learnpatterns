<?php

require 'Classes/Window.php';
require 'Classes/WindowBuilder.php';

use Builder\WindowBuilder;

//Test use Builder

$windowBuilder = new WindowBuilder();
$window = $windowBuilder->setVisible(true)
    ->setTitle("New Window !!!")
    ->setWidth(540)
    ->setHeight(360)
    ->build();

$window->printProperties();

?>