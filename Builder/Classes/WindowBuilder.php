<?php

namespace Builder;

class WindowBuilder
{
    private $title;
    private $width;
    private $height;
    private $visible;

    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }

    public function setWidth($value)
    {
        $this->width = $value;
        return $this;
    }

    public function setHeight($value)
    {
        $this->height = $value;
        return $this;
    }

    public function setVisible($value)
    {
        $this->visible = $value;
        return $this;
    }

    public function build()
    {
        return new Window($this->title, $this->width, $this->height, $this->visible);
    }

}