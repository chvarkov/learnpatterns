<?php

namespace Builder;

class Window
{
    public $title = "Window";
    public $width = 360;
    public $height = 180;
    public $visible = false;

    public function __construct($title, $width, $height, $visible)
    {
        $this->title = $title;
        $this->width = $width;
        $this->height = $height;
        $this->visible = $visible;
    }

    public function printProperties()
    {
        print_r($this);
    }
}

?>