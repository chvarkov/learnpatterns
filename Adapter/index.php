<?php

require 'Interfaces/AlphaBankPayment.php';
require 'Interfaces/BettaBankPayment.php';
require 'Interfaces/AdapterBankInterface.php';
require 'Classes/AlphaBank.php';
require 'Classes/BettaBank.php';
require 'Classes/AlphaBankAdapter.php';
require 'Classes/BettaBankAdapter.php';
require 'Classes/UnitedPaymentSystem.php';

use Adapter\AlphaBank\AlphaBank;
use Adapter\BettaBank\BettaBank;
use Adapter\UnitedPaymentSystem;

// Test AlphaBank
echo "\n================ Alpha Bank ====================\n";
$alphaBank = new AlphaBank();
$alphaBank->payToAlphaBank("785738543", 12);
$alphaBank->payoutFromAlphaBank("7847526765", 17);

// Test BettaBank
echo "\n================ Betta Bank ====================\n";
$bettaBank = new BettaBank();
$bettaBank->payToBettaBank("785738543", 43);
$bettaBank->payoutFromBettaBank("7847526765", 25);

echo "\n================ Testing United payment system ====================\n";

// Test Adapter

$ups = new UnitedPaymentSystem();

//Pay to AlphaBank
$ups->pay(UnitedPaymentSystem::ALPHA_BANK, "5435345657", 12.45);
//Pay to BettaBank
$ups->pay(UnitedPaymentSystem::BETTA_BANK, "5435345657", 16.38);

//Payout from AlphaBank
$ups->payout(UnitedPaymentSystem::ALPHA_BANK, "5435345657", 15.5);
//Payout from BettaBank
$ups->payout(UnitedPaymentSystem::BETTA_BANK, "5435345657", 17.45);

?>