<?php

namespace Adapter;

class UnitedPaymentSystem
{
    //Banks
    const ALPHA_BANK = "AlphaBank";
    const BETTA_BANK = "BettaBank";

    private $alphaBankAdapter;
    private $bettaBankAdapter;

    public function __construct()
    {
        $this->alphaBankAdapter = new AlphaBankAdapter();
        $this->bettaBankAdapter = new BettaBankAdapter();
    }

    private function getBankAdapter($bankName)
    {
        switch ($bankName)
        {
            case UnitedPaymentSystem::ALPHA_BANK: return $this->alphaBankAdapter;
            case UnitedPaymentSystem::BETTA_BANK: return $this->bettaBankAdapter;
            default: return null;
        }
    }

    public function pay($bankName, $cardNumber, $amount)
    {
        $bankAdapter = $this->getBankAdapter($bankName);
        if ($bankAdapter != null)
            $bankAdapter->pay($cardNumber, $amount);
        else
            echo "\nError!. $bankName not exist.\n";
    }

    public function payout($bankName, $cardNumber, $amount)
    {
        $bankAdapter = $this->getBankAdapter($bankName);
        if ($bankAdapter != null)
            $bankAdapter->payout($cardNumber, $amount);
        else
            echo "\nError!. $bankName not exist.\n";
    }


}