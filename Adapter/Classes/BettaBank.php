<?php

namespace Adapter\BettaBank;

class BettaBank implements BettaBankPayment
{
    public $bankName = "BettaBank";
    protected $cardNumberBank = "8226428284";

    public function payoutFromBettaBank($cardNumber, $amount)
    {
        echo "\nSending $amount$, on $cardNumber from $this->bankName ($this->cardNumberBank)\n";
    }

    public function payToBettaBank($cardNumber, $amount)
    {
        echo "\nSending $amount$, on $this->bankName ($this->cardNumberBank) from $cardNumber \n";
    }
}

?>