<?php

namespace Adapter\AlphaBank;

use Adapter\AlphaBank\AlphaBankPayment;

class AlphaBank implements AlphaBankPayment
{
    public $bankName = "AlphaBank";

    protected $cardNumberBank = "1573197351";

    public function payoutFromAlphaBank($cardNumber, $amount)
    {
        echo "\nSending $amount$, on $cardNumber from $this->bankName ($this->cardNumberBank)\n";
    }

    public function payToAlphaBank($cardNumber, $amount)
    {
        echo "\nSending $amount$, on $this->bankName ($this->cardNumberBank) from $cardNumber \n";
    }

}

?>