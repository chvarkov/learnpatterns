<?php

namespace Adapter;

class AlphaBankAdapter implements AdapterBankInterface
{
    private $bank;

    public function __construct()
    {
        $this->bank = new AlphaBank\AlphaBank();
    }

    public function pay($cardNumber, $amount)
    {
        $this->bank->payToAlphaBank($cardNumber, $amount);

    }

    public function payout($cardNumber, $amount)
    {
        $this->bank->payoutFromAlphaBank($cardNumber, $amount);
    }

}

?>