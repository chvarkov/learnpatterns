<?php

namespace Adapter;

use Adapter\AdapterBankInterface;

class BettaBankAdapter implements AdapterBankInterface
{
    private $bank;

    public function __construct()
    {
        $this->bank = new BettaBank\BettaBank();
    }

    public function pay($cardNumber, $amount)
    {
        $this->bank->payToBettaBank($cardNumber, $amount);

    }

    public function payout($cardNumber, $amount)
    {
        $this->bank->payoutFromBettaBank($cardNumber, $amount);
    }

}

?>