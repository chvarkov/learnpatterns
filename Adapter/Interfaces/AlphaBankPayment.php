<?php

namespace Adapter\AlphaBank;

interface AlphaBankPayment
{
    public function payToAlphaBank($cardNumber, $amount);
    public function payoutFromAlphaBank($cardNumber, $amount);
}

?>