<?php

namespace Adapter;

interface AdapterBankInterface
{
    const ALPHA_BANK = "AlphaBank";
    const BETTA_BANK = "BettaBank";

    public function pay($cardNumber, $amount);
    public function payout($cardNumber, $amount);
}

?>