<?php

namespace Adapter\BettaBank;

interface BettaBankPayment
{
    public function payToBettaBank($cardNumber, $amount);
    public function payoutFromBettaBank($cardNumber, $amount);
}

?>