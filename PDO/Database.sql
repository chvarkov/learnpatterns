-- phpMyAdmin SQL Dump
-- version 4.6.6deb5
-- https://www.phpmyadmin.net/
--
-- Хост: localhost:3306
-- Время создания: Июн 29 2018 г., 22:06
-- Версия сервера: 5.7.22-0ubuntu18.04.1
-- Версия PHP: 7.2.5-0ubuntu0.18.04.1

SET SQL_MODE = "NO_AUTO_VALUE_ON_ZERO";
SET time_zone = "+00:00";


/*!40101 SET @OLD_CHARACTER_SET_CLIENT=@@CHARACTER_SET_CLIENT */;
/*!40101 SET @OLD_CHARACTER_SET_RESULTS=@@CHARACTER_SET_RESULTS */;
/*!40101 SET @OLD_COLLATION_CONNECTION=@@COLLATION_CONNECTION */;
/*!40101 SET NAMES utf8mb4 */;

--
-- База данных: `learnPDO`
--

-- --------------------------------------------------------

--
-- Структура таблицы `News`
--

CREATE TABLE `News` (
  `NewsId` bigint(20) UNSIGNED NOT NULL,
  `Title` varchar(255) CHARACTER SET utf16 NOT NULL,
  `Text` text NOT NULL,
  `Date` timestamp NOT NULL DEFAULT CURRENT_TIMESTAMP
) ENGINE=InnoDB DEFAULT CHARSET=utf8;

--
-- Дамп данных таблицы `News`
--

INSERT INTO `News` (`NewsId`, `Title`, `Text`, `Date`) VALUES
(1, 'Title for news 1', 'Text for news 1', '2018-06-29 19:04:46'),
(2, 'Title for news 2', 'Text for news 2', '2018-06-29 19:06:01'),
(3, 'Title for news 3', 'Text for news 3', '2018-06-29 19:06:02'),
(4, 'Title for news 4', 'Text for news 4', '2018-06-29 19:06:02'),
(5, 'Title for news 5', 'Text for news 5', '2018-06-29 19:06:02'),
(6, 'Title for news 6', 'Text for news 6', '2018-06-29 19:06:02'),
(7, 'Title for news 7', 'Text for news 7', '2018-06-29 19:06:02'),
(8, 'Title for news 8', 'Text for news 8', '2018-06-29 19:06:02');

--
-- Индексы сохранённых таблиц
--

--
-- Индексы таблицы `News`
--
ALTER TABLE `News`
  ADD PRIMARY KEY (`NewsId`);

--
-- AUTO_INCREMENT для сохранённых таблиц
--

--
-- AUTO_INCREMENT для таблицы `News`
--
ALTER TABLE `News`
  MODIFY `NewsId` bigint(20) UNSIGNED NOT NULL AUTO_INCREMENT, AUTO_INCREMENT=9;
/*!40101 SET CHARACTER_SET_CLIENT=@OLD_CHARACTER_SET_CLIENT */;
/*!40101 SET CHARACTER_SET_RESULTS=@OLD_CHARACTER_SET_RESULTS */;
/*!40101 SET COLLATION_CONNECTION=@OLD_COLLATION_CONNECTION */;
