<?php

require 'Classes/DB.php';
require 'Classes/News.php';
require 'Classes/Response.php';


if (isset($_GET["id"]))
{
    $id = $_GET["id"];
    $news = DB::getNewsById($id);
    if ($news != null) {
        $answer = true;
        if (isset($_GET["title"]))
            $answer = $answer == $news->setTitle($_GET["title"]);
        if (isset($_GET["text"]))
            $answer = $answer == $news->setText($_GET["text"]);
        $response = new Response($answer);
        $response->sendOut();
    }
    else
        Response::error("Object News is not valid.");
}
else
    Response::error("Incorrect paramentrs.");

?>