<?php

class DB
{

    const TABLE_NEWS = "News";

    private static $tables = array(self::TABLE_NEWS);

    private static $host = "127.0.0.1:3306";
    private static $dbname = "learnPDO";
    private static $user = "admin";
    private static $password = "root";

    private static $pdo;

    private function __construct() {}

    private function __clone() {}

    //For News

    public static function removeNewsById($id)
    {
        try {
            return self::execute("DELETE FROM News WHERE NewsId = $id") !== false;
        } catch (PDOException $e) {
            Response::error($e->errorInfo);
        }
    }

    public static function setNewsValue($column, $value, $id)
    {
        try {
            $value = DB::getPDO()->quote($value);
            return self::execute("UPDATE News SET $column = $value WHERE NewsId = $id") !== false;
        }
        catch (PDOException $e) {
            Response::error($e->errorInfo);
        }
    }


    public static function getNews($limit = 0, $offset = 0)
    {
        $sql = "SELECT * FROM News".(($limit > 0)? (" LIMIT $limit" .(($offset > 0)? ", $offset " : "")) : "");
        try {
            $result = DB::query($sql, PDO::FETCH_ASSOC);
            $arr = array();
            foreach ($result as $item)
                $arr[] = new News($item);
            return $arr;
        }
        catch(PDOException $e) {
            Response::error($e->errorInfo);
        }
        catch (Exception $e) {
            Response::error($e->getMessage());
        }
    }

    public static function addNews($title, $text)
    {
        try {
            $pdo = DB::getPDO();
            $title = $pdo->quote($title);
            $text = $pdo->quote($text);
            return DB::execute("INSERT INTO News(Title, Text) VALUES($title, $text)") !== false;
        }
        catch(PDOException $e) {
            Response::error($e->errorInfo);
        }
        catch (Exception $e) {
            Response::error($e->getMessage());
        }
    }

    public static function getNewsById($id)
    {
        try {
            $news = DB::query("SELECT * FROM News WHERE NewsId = $id");
            $fetсhArray = $news->fetch();
            return (!empty($fetсhArray)) ? new News($fetсhArray) : null;
        } catch (PDOException $e) {
            Response::error($e->errorInfo);
        } catch (Exception $e) {
            Response::error($e->getMessage());
        }
    }

    public static function query($sql)
    {
        try {
            return DB::getPDO()->query($sql, PDO::FETCH_ASSOC);
        }
        catch (PDOException $e){
            Response::error($e->errorInfo);
        }
    }

    public static function execute($sql)
    {
        try {
            return DB::getPDO()->exec($sql);
        }
        catch (PDOException $e) {
            Response::error($e->errorInfo);
        }
    }

    public static function getPDO()
    {
        if (self::$pdo == null) {
            self::$pdo = new PDO("mysql:host=" . self::$host . ";dbname=" . self::$dbname, self::$user, self::$password);
            self::$pdo->setAttribute(PDO::ATTR_ERRMODE, PDO::ERRMODE_EXCEPTION);
        }
        return self::$pdo;
    }
}

?>