<?php

class Response
{
    public $response;

    public function __construct($object = null)
    {
        $this->response = $object;
    }

    public static function error($message)
    {
        $response = new Response();
        $response->response = new stdClass();
        $response->response->error = $message;
        $response->sendOut();
    }

    public function sendOut($object = null)
    {
        $this->response = $object === null? $this->response : $object;
        header("Content-type: text/json; charset: utf8;");
        echo json_encode($this);
        exit;
    }

}

?>