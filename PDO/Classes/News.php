<?php

class News
{
    const COL_NEWSID = "NewsId";
    const COL_TITLE = "Title";
    const COL_TEXT = "Text";
    const COL_DATE = "Date";

    public $newId;
    public $title;
    public $text;
    public $date;

    public function __construct($arrayFetch)
    {
        $this->newId = $arrayFetch["NewsId"];
        $this->title = $arrayFetch["Title"];
        $this->text = $arrayFetch["Text"];
        $this->date = $arrayFetch["Date"];
    }

    public function remove()
    {
        return DB::removeNewsById($this->newId);
    }

    //Getters and setters

    public function getNewId()
    {
        return $this->newId;
    }

    public function getTitle()
    {
        return $this->title;
    }

    public function getText()
    {
        return $this->text;
    }

    public function getDate()
    {
        return $this->date;
    }

    public function setTitle($title)
    {
        return DB::setNewsValue(News::COL_TITLE, $title, $this->newId) !== false;
    }

    public function setText($text)
    {
        return DB::setNewsValue(News::COL_TEXT, $text, $this->newId) !== false;
    }
}

?>