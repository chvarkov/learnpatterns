<?php

require 'Classes/DB.php';
require 'Classes/News.php';
require 'Classes/Response.php';

if (isset($_GET["id"]))
{
    $id = $_GET["id"];
    $response = new Response(DB::removeNewsById($id));
    $response->sendOut();
}
else
    Response::error("Object News is not valid.")

?>