<?php

require 'Classes/DB.php';
require 'Classes/News.php';
require 'Classes/Response.php';

$limit = isset($_GET["limit"]) ? (int)$_GET["limit"] : 0;
$limit = $limit >= 0 ? $limit : 0;

$offset = isset($_GET["offset"]) ? (int)$_GET["offset"] : 0;
$offset = $offset >= 0 ? $offset : 0;

$response = new Response(DB::getNews($limit,$offset));
$response->sendOut();

?>