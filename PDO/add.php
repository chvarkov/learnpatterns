<?php

require 'Classes/DB.php';
require 'Classes/News.php';
require 'Classes/Response.php';

if (isset($_GET["title"]) && isset($_GET["text"]))
{
    $title = $_GET["title"];
    $text = $_GET["text"];
    $response = new Response(DB::addNews($title, $text));
    $response->sendOut();
}
else
    Response::error("Invalid parametrs");

?>