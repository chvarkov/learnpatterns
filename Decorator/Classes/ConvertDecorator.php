<?php

namespace Decorator;

use Strategy\ConvertStrategy;

abstract class ConvertDecorator implements ConvertStrategy
{
    protected  $decoratedConverter;

    protected function __construct(ConvertStrategy $decoratedConvert)
    {
        $this->decoratedConverter = $decoratedConvert;
    }

    public function convert($object)
    {
        return $this->decoratedConverter->convert($object);
    }
}