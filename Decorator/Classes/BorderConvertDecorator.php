<?php

namespace Decorator;

use Strategy\ConvertStrategy;

class BorderConvertDecorator extends ConvertDecorator
{
    public function __construct(ConvertStrategy $decoratedConvert)
    {
        parent::__construct($decoratedConvert);
    }

    public function printTopBorder()
    {
        echo "========== CONVERTED ==========\n";
    }

    public function printBottomBorder()
    {
        echo "\n===============================\n";
    }

    public function convert($object)
    {
        $this->printTopBorder();
        $convertedObject = parent::convert($object);
        print_r($convertedObject);
        $this->printBottomBorder();
    }

}