<?php

require '../Strategy/Interfaces/ConvertStrategy.php';
require '../Strategy/Classes/Strategies/PHP.php';
require '../Strategy/Classes/Strategies/AssocArray.php';
require '../Strategy/Classes/Strategies/XML.php';
require '../Strategy/Classes/Strategies/JSON.php';
require '../Strategy/Classes/ForExample/User.php';
require 'Classes/ConvertDecorator.php';
require 'Classes/BorderConvertDecorator.php';

use Strategy\Strategies\PHP;
use Strategy\Strategies\AssocArray;
use Strategy\Strategies\XML;
use Strategy\Strategies\JSON;
use Strategy\ForExample\User;
use Decorator\BorderConvertDecorator;

//Test use Decorator

$exampleObject = new User(1, "admin", "admin@domain.com", "123123");
$exampleObject->addPost("This first post");
$exampleObject->addPost("This second ...");

//Convert to php + border
$borderConverter = new BorderConvertDecorator(new PHP());
$borderConverter->convert($exampleObject);
//Convert to associative array + border
$borderConverter = new BorderConvertDecorator(new AssocArray());
$borderConverter->convert($exampleObject);
//Convert to xml format + border
$borderConverter = new BorderConvertDecorator(new XML());
$borderConverter->convert($exampleObject);
//Convert to json format + border
$borderConverter = new BorderConvertDecorator(new JSON());
$borderConverter->convert($exampleObject);


?>