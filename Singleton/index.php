<?php

require 'Classes/Logger.php';

use Singleton\Logger;

//Test use Singleton

$logger = Logger::getInstance();
$logger->setFileName("MyLog.log");
$logger->log("All right!");

$logger2 = Logger::getInstance();
$logger2->log("Yes! All right!");

?>