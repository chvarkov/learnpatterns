<?php

namespace Singleton;

class Logger
{
    private $dir = "Logs";
    private $filename = "log.log";
    static private $instance; //Singleton object

    private function __construct() { }
    private function __clone() { }

    static public function getInstance()
    {
        if (!self::$instance)
            self::$instance = new self;
        return self::$instance;
    }

    /**
     * Set name for log file
     * @param $filename
     * @return Logger
     */
    public function setFileName($filename)
    {
        $this->filename = $filename;
        return $this;
    }

    /**
     * Set directory for log file
     * @param $dir
     * @return Logger
     */
    public function setDir($dir)
    {
        $this->dir = $dir;
        return $this;
    }

    /**
     * Print message in log file
     * @param $textMessage
     */
    public function log($textMessage)
    {
        $message = date("h:i:s d.m.Y", time())." | $textMessage\n\n";
        $path = "$this->dir/$this->filename";
        file_put_contents($path, $message, FILE_APPEND);
    }
}