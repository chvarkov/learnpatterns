<?php

require 'Classes/Product.php';
require 'Classes/ProductFactory.php';
require 'Classes/Products/Laptop.php';
require 'Classes/Products/Printer.php';
require 'Classes/Products/TV.php';

use FactoryMethod\ProductFactory;
use FactoryMethod\Products\Laptop;
use FactoryMethod\Products\Printer;
use FactoryMethod\Products\TV;

//Test use Factory Method

$productFactory = new ProductFactory();

$laptop = $productFactory->getProduct(Laptop::class);
$laptop->setTitle("Laptop ASUS")
    ->setDescription("New model laptop ASUS")
    ->setCost(750)
    ->setBrand("ASUS")
    ->setModel("K53S")
    ->setDiagonal(19.5)
    ->setDriveMemory(500)
    ->setProcessor("Intel I5")
    ->setRAM(4);
$laptop->show();

$printer = $productFactory->getProduct(Printer::class);
$printer->setTitle("Printer HP")
    ->setDescription("The best printer 2018")
    ->setCost(250)
    ->setBrand("HP")
    ->setModel("Laser 5000 JET")
    ->setTypePrint("Laser")
    ->setMaxFormatPaper("A4")
    ->setPrintColors("Black-white")
    ->setPrintSpeed(40);
$printer->show();

$tv = $productFactory->getProduct(TV::class);
$tv->setTitle("TV LG")
    ->setDescription("New model TV by LG")
    ->setCost(500)
    ->setBrand("LG")
    ->setModel("TV 3000")
    ->setDiagonal(32);
$tv->show();

?>