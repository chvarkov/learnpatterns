<?php

namespace FactoryMethod\Products;


use FactoryMethod\Product;

class TV extends Product
{
    protected $diagonal;

    public function __construct($title = null, $description = null, $cost = null, $brand = null,
        $model = null, $diagonal = null)
    {
        parent::__construct($title, $description, $cost, $brand, $model);
        $this->diagonal = $diagonal;
    }

    public  function show()
    {
        print_r($this);
    }

    //Getters and setters

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function setDiagonal($value)
    {
        $this->diagonal = $value;
        return $this;
    }
}