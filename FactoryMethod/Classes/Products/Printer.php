<?php

namespace FactoryMethod\Products;


use FactoryMethod\Product;

class Printer extends Product
{
    protected $typePrint;
    protected $maxFormatPaper;
    protected $printColors;
    protected $printSpeed;

    public function __construct($title = null, $description = null, $cost = null, $brand = null,
        $model = null, $typePrint = null, $maxFormatPaper = null, $printColors = null, $printSpeed = null)
    {
        parent::__construct($title, $description, $cost, $brand, $model);
        $this->typePrint = $typePrint;
        $this->maxFormatPaper = $maxFormatPaper;
        $this->printColors = $printColors;
        $this->printSpeed = $printSpeed;
    }

    public function show()
    {
        print_r($this);
    }

    //Getters and Setters

    public function getTypePrint()
    {
        return $this->typePrint;
    }

    public function setTypePrint($value)
    {
        $this->typePrint = $value;
        return $this;
    }

    public function getMaxFormatPaper()
    {
        return $this->maxFormatPaper;
    }

    public function setMaxFormatPaper($value)
    {
        $this->maxFormatPaper = $value;
        return $this;
    }

    public function getPrintColors()
    {
        return $this->printColors;
    }

    public function setPrintColors($value)
    {
        $this->printColors = $value;
        return $this;
    }

    public function getPrintSpeed()
    {
        return $this->printSpeed;
    }

    public function setPrintSpeed($value)
    {
        $this->printSpeed = $value;
        return $this;
    }
}