<?php

namespace FactoryMethod\Products;

use FactoryMethod\Product;

class Laptop extends Product
{
    protected $ram;
    protected $driveMemory;
    protected $processor;
    protected $diagonal;

    public function __construct($title = null, $description = null, $cost = null, $brand = null,
        $model = null, $ram = null, $driveMemory = null, $processor = null, $diagonal = null)
    {
        parent::__construct($title, $description, $cost, $brand, $model);
        $this->ram = $ram;
        $this->driveMemory = $driveMemory;
        $this->processor = $processor;
        $this->diagonal = $diagonal;
    }

    public function show()
    {
        print_r($this);
    }

    //Getters and setters

    public function getRAM()
    {
        return $this->ram;
    }

    public function setRAM($value)
    {
        $this->ram = $value;
        return $this;
    }

    public function getDriveMemory()
    {
        return $this->driveMemory;
    }

    public function setDriveMemory($value)
    {
        $this->driveMemory = $value;
        return $this;
    }

    public function getProcessor()
    {
        return $this->processor;
    }

    public function setProcessor($value)
    {
        $this->processor = $value;
        return $this;
    }

    public function getDiagonal()
    {
        return $this->diagonal;
    }

    public function setDiagonal($value)
    {
        $this->diagonal = $value;
        return $this;
    }
}

?>