<?php

namespace FactoryMethod;

abstract class Product
{
    protected $title;
    protected $description;
    protected $cost;
    protected $brand;
    protected $model;

    abstract public function show();

    protected function __construct($title, $description, $cost, $brand, $model)
    {
        $this->title = $title;
        $this->description = $description;
        $this->cost = $cost;
        $this->brand = $brand;
        $this->model = $model;
    }

    //Getters and setters

    public function getTitle()
    {
        return $this->title;
    }

    public function setTitle($value)
    {
        $this->title = $value;
        return $this;
    }

    public function getDescription()
    {
        return $this->description;
    }

    public function setDescription($value)
    {
        $this->description = $value;
        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($value)
    {
        $this->cost = $value;
        return $this;
    }

    public function getBrand()
    {
        return $this->brand;
    }

    public function setBrand($value)
    {
        $this->brand = $value;
        return $this;
    }

    public function getModel()
    {
        return $this->model;
    }

    public function setModel($value)
    {
        $this->model = $value;
        return $this;
    }
}

?>