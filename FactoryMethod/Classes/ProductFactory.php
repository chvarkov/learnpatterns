<?php

namespace FactoryMethod;

use FactoryMethod\Products\Laptop;
use FactoryMethod\Products\Printer;
use FactoryMethod\Products\TV;

class ProductFactory
{
    public function getProduct($className)
    {
        switch ($className)
        {
            case Laptop::class: return new Laptop();
            case Printer::class: return new Printer();
            case TV::class: return new TV();
            default: throw new \Exception("Not found class: $className");
        }
    }
}