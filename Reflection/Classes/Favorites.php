<?php

namespace Reflection;

use Reflection\Plugins;

class Favorites
{

    private $plugins = [];

    public function __construct()
    {
        $isExists = false;
        foreach (glob("Classes/Plugins/*/*.plugin.php") as $item)
        {
            if (is_file($item)) {
                include_once  "$item";
                $isExists = true;
            }
        }
        if ($isExists)
            $this->findPlugins();
    }

    public function findPlugins()
    {
        foreach (get_declared_classes() as $class) {
            $rClass = new \ReflectionClass($class);
            if ($rClass->implementsInterface("Reflection\IPlugin"))
                $this->plugins[] = $rClass;
        }
    }

    private function getItems($methodName)
    {
        $list = [];
        if (!empty($this->plugins))
        {
            foreach ($this->plugins as $plugin)
            {
                if ($plugin->hasMethod($methodName))
                {
                    $rm = new \ReflectionMethod($plugin->name, $methodName);
                    if ($rm->isPublic())
                    {
                        if ($rm->isStatic())
                            $list[] = $rm->invoke(null);
                        else
                            $list[] = $rm->invoke($plugin->newInstance());
                    }
                }

            }
            return $list;
        }
        else
            return array();
    }

    public function getLinks()
    {
        return $this->getItems(__FUNCTION__);
    }

    public function getApps()
    {
        return $this->getItems(__FUNCTION__);
    }

    public function getArticles()
    {
        return $this->getItems(__FUNCTION__);
    }
}