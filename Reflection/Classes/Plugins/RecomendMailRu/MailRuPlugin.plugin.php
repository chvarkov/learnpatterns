<?php

namespace Reflection\Plugins;

use Reflection\IPlugin;

class MainRuPlugin implements IPlugin
{
    public static function getLinks()
    {
        return array(
            "MailRu Link1" => "http://",
            "MailRu Link2" => "http://"
        );
    }

    public static function getApps()
    {
        return array(
            "MailRu App1" => "http://",
            "MailRu App2" => "http://"
        );
    }

    public static function getArticles()
    {
        return array(
            "MailRu Arcticle1" => "http://",
            "MailRu Arcticle2" => "http://",
            "MailRu Arcticle3" => "http://"
        );
    }
}

?>