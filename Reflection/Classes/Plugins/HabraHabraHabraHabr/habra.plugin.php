<?php

namespace Reflection\Plugins;

use Reflection\IPlugin;

class HabraRecomendation implements IPlugin
{
    public static function getLinks()
    {
        return array("Habrahabr" => "http://");
    }

    public function getArticles()
    {
        return array(
            "Habr 1" => "http://",
            "Habr 2" => "http://",
            "Habr 3" => "http://",
            "Habr 4" => "http://",
            "Habr 5" => "http://",
            "Habr 6" => "http://",
            "Habr 7" => "http://"
        );
    }
}

?>