<?php

namespace Reflection\Plugins;

use Reflection\IPlugin;

class MicrosoftPlugin implements IPlugin
{
    public static function getLinks()
    {
        return include("Favorites/WebSites.php");
    }

    public function getApps()
    {
        return include("Favorites/Apps.php");
    }

    public function getArticles()
    {
        return include ("Favorites/Articles.php");
    }
}

?>