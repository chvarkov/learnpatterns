<?php

require 'Classes/IPlugin.interface.php';
require 'Classes/Favorites.php';

use Reflection\Favorites;

$favorites = new Favorites();

?>


<table width="50%" align="center">
    <tr>
        <th colspan="3"><h1>Favorites</h1></th>
    </tr>
    <tr>
        <th><h3>Links</h3></th>
        <th><h3>Applications</h3></th>
        <th><h3>Articles</h3></th>
    </tr>
    <tr align="left" valign="top">
        <th>
            <ul>
                <?php
                foreach ($favorites->getLinks() as $links)
                    foreach ($links as $title => $link):
                ?>
                    <li><a href="<?php echo $link ?>"><?php echo $title ?></a></li>
                <?php endforeach; ?>
            </ul>
        </th>
        <th>
            <ul>
                <?php
                foreach ($favorites->getApps() as $apps)
                    foreach ($apps as $app => $link):
                ?>
                    <li><a href="<?php echo $link ?>"><?php echo $app ?></a></li>
                <?php endforeach; ?>
            </ul>
        </th>
        <th>
            <ul>
                <?php
                foreach ($favorites->getArticles() as $articles)
                foreach ($articles as $article => $link):
                ?>
                    <li><a href="<?php echo $link ?>"><?php echo $article ?></a></li>
                <?php endforeach; ?>
            </ul>
        </th>
    </tr>
</table>
