<?php

$priorityQueue = new SplPriorityQueue();

$priorityQueue->insert("Apple", 5);
$priorityQueue->insert("Orange", 2);
$priorityQueue->insert("Banana", 7);
$priorityQueue->insert("Lemon", 1);

echo "PRIORITY STACK. Fruit output by like:<br/>";
foreach ($priorityQueue as $value)
    echo "$value<br/>";

?>