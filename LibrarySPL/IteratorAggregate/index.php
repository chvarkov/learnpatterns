<?php

require '../Iterator/Classes/SquareIterator.php';
require '../Iterator/Classes/IncIterator.php';
require '../Iterator/Classes/DecIterator.php';
require 'Classes/MathIterator.php';

use IteratorAggragate\MathIterator;

//Test use interface IteratorAggregate

$math = new MathIterator(MathIterator::POW2, 4, 9);
echo "Square: \n";
foreach ($math as $value)
    echo "$value\n";

$math = new MathIterator(MathIterator::INC, 7, 12);
echo "Increment: \n";
foreach ($math as $value)
    echo "$value\n";

$math = new MathIterator(MathIterator::DEC, 3, 8);
echo "Decrement: \n";
foreach ($math as $value)
    echo "$value\n";

$math = new MathIterator("Кракозябра", 7, 12);
echo "Faled:\n";
foreach ($math as $value)
    echo "$value\n";


?>