<?php

namespace IteratorAggragate;

use Iterator\IncIterator;
use Iterator\DecIterator;
use Iterator\SquareIterator;

class MathIterator implements \IteratorAggregate
{
    const INC = 1;
    const DEC = 2;
    const POW2 = 3;

    private $iterator;

    public function __construct($mathFlag, $fromValue, $toValue)
    {
        switch ($mathFlag)
        {
            case MathIterator::INC: $this->iterator = new IncIterator($fromValue, $toValue); break;
            case MathIterator::DEC: $this->iterator = new DecIterator($fromValue, $toValue); break;
            case MathIterator::POW2: $this->iterator = new SquareIterator($fromValue, $toValue); break;
            default: throw new \Exception("This flag: $mathFlag is not exists");
        }
    }

    public function getIterator()
    {
        return $this->iterator;
    }
}

?>