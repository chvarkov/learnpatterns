<?php

//Test use LimitIterator for pagination

$array = ["BMW M5", "Audi A6", "Toyota Corolla", "KIA RIO",
            "Mazda 6", "Opel Vectra", "Skoda Octavia"
];

$arrayIterator = new ArrayIterator($array);
$countItemsOnPage = 3;
$currentPage = 2;

$offset = $currentPage * $countItemsOnPage - $countItemsOnPage;
echo "CURRENT PAGE $currentPage:\n";
try {
    foreach (new LimitIterator($arrayIterator, $offset, $countItemsOnPage) as $item)
        echo $item . "\n";
}
catch (OutOfBoundsException $e){
    echo "Нет записей\n";
}
catch (Exception $e)
{
    echo "Error: ".$e->getMessage();
}

?>