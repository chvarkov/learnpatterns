<?php

require 'Classes/Product.php';
require 'Classes/Book.php';
require 'Classes/BookCollection.php';

use Serializable\Book;
use Serializable\BookCollection;

//Test interface Serializable

// Ok, serialize object Product it's easy, try serialize object Book extends Product

//Serializable object Book
/*
$book = new Book("Думай и богатей", 7, "Наполеон Хилл", 548);
$book->dump();
$serializeString = $book->serialize();
echo "Serialize string: $serializeString\n";

//Create empty Book and unserialize from $serializeString
$newBook = new Book();
$newBook->unserialize($serializeString);
$newBook->dump();
*/

//Now and this easy, try serialize object BookCollection

$arrayBooks = array(
    new Book("Book 1", 7, "Author 1", 548),
    new Book("Book 2", 6, "Author 2", 648),
    new Book("Book 3", 5, "Author 3", 748)
);

$books = new BookCollection($arrayBooks);
var_dump($books);
$serializeString = $books->serialize();

echo "STRING: $serializeString";
echo "\n======== unserialize =========\n";

$books2 = new BookCollection();
$books2->unserialize($serializeString);
var_dump($books2);


?>