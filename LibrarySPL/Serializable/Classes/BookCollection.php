<?php

namespace Serializable;


class BookCollection extends \ArrayIterator implements \Serializable
{
    public function __construct(array $array = array(), int $flags = 0)
    {
        parent::__construct($array, $flags);
    }

    public function serialize()
    {
        return serialize($this->getArrayCopy());
    }

    public function unserialize($serialized)
    {
        $this->append(unserialize($serialized));
    }

}