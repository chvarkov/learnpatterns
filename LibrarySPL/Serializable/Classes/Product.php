<?php

namespace Serializable;


class Product implements \Serializable
{
    private $name;
    private $cost;

    public function __construct($name = null, $cost = null)
    {
        $this->name = $name;
        $this->cost = $cost;
    }

    //Implement Serializable

    public function serialize()
    {
        return serialize([$this->name, $this->cost]);
    }

    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        $this->name = $data[0];
        $this->cost = $data[1];
    }


    //Getters and setters

    public function getName()
    {
        return $this->name;
    }

    public function setName($value)
    {
        $this->name = $value;
        return $this;
    }

    public function getCost()
    {
        return $this->cost;
    }

    public function setCost($value)
    {
        $this->cost = $value;
        return $this;
    }
}