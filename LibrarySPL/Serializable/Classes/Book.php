<?php

namespace Serializable;

use Serializable\Product;

class Book extends Product implements \Serializable
{
    private $author;
    private $countPages;

    public function __construct($name = null, $cost = null, $author = null, $countPages = null)
    {
        parent::__construct($name, $cost);
        $this->author = $author;
        $this->countPages = $countPages;
    }

    public function dump()
    {
        var_dump($this);
    }

    public function serialize()
    {
        return serialize([parent::serialize(), $this->author, $this->countPages]);
    }

    public function unserialize($serialized)
    {
        $data = unserialize($serialized);
        parent::unserialize($data[0]);
        $this->author = $data[1];
        $this->countPages = $data[2];
    }

    //Getters and setters

    public function getAuthor()
    {
        return $this->author;
    }

    public function setAuthor($value)
    {
        $this->author = $value;
        return $this;
    }

    public function getCountPages()
    {
        return $this->countPages;
    }

    public function setCountPages($value)
    {
        $this->countPages = $value;
        return $this;
    }

}