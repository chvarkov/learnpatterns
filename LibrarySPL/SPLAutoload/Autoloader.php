<?php
/*
namespace Project;

class Autoloader
{


    public static function load()
    {
        spl_autoload_register(['Project\\Autoloader', 'loadClasses'], false);
        spl_autoload_register(['Project\\Autoloader', 'loadInterfaces'], false);
    }
}

Autoloader::load();
*/

class Autoloader
{

    public static function getName($classname)
    {
        $items = explode("\\", $classname);
        return array_pop($items);
    }

    public static function loadClasses($classname)
    {
        require_once "Classes/" . self::getName($classname) . ".php";
    }

    public static function loaded()
    {
        spl_autoload_register(['Autoloader', 'loadClasses']);
    }
}



?>