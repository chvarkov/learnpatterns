<?php

require 'Classes/News.php';
require 'Classes/NewsArrayIterator.php';

use ArrayIterator\News;
use ArrayIterator\NewsArrayIterator;

$news = array();
for ($i = 1; $i <= 5; $i++)
    $news[] = new News($i, "New $i", "Text $i", new DateTime("now"));
$newsArrayIterator = new NewsArrayIterator($news);

$newsArrayIterator->append(new News(8, "Append new", "text...", new DateTime("now")));

foreach ($newsArrayIterator as $item)
{
    var_dump($item);
}

print_r($newsArrayIterator->getArrayCopy());


?>