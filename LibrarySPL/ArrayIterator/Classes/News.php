<?php

namespace ArrayIterator;

class News
{
    private $id, $title, $text, $date;

    public function __construct(int $id = null, string $title = null, string $text = null, \DateTime $date = null)
    {
        $this->id = $id;
        $this->title = $title;
        $this->text = $text;
        $this->date = $date;
    }

    public function showJSON()
    {
        echo json_encode($this)."\n";
    }

    public function getId() : int
    {
        return (int) $this->id;
    }

    public function setId(int $id)
    {
        $this->id = $id;
        return $this;
    }

    public function getText() : string
    {
        return (string) $this->text;
    }

    public function setText(string $text)
    {
        $this->text = $text;
        return $this;
    }

    public function getTitle() : string
    {
        return (string) $this->title;
    }

    public function setTitle(string $title)
    {
        $this->title = $title;
        return $this;
    }

    public function getDate() : \DateTime
    {
        return $this->date;
    }

    public function setDate(\DateTime $date)
    {
        $this->date = $date;
        return $this;
    }

}

?>