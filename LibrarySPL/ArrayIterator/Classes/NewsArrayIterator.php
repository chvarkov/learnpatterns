<?php

namespace ArrayIterator;


class NewsArrayIterator extends \ArrayIterator
{
    public function __construct(array $array = array())
    {
        parent::__construct($array, NewsArrayIterator::STD_PROP_LIST);
    }
}