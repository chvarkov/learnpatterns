<?php

require 'Classes/Random10.php';

use AppendIterator\Random10;

//Test use AppendIterator

$ai = new AppendIterator();

$object1 = new Random10();
$object2 = new Random10();
$object3 = new Random10();

$ai->append(new ArrayIterator([$object1, $object2, $object3]));

$object4 = new Random10();
$object5 = new Random10();

$ai->append(new ArrayIterator([$object4, $object5]));

foreach ($ai as $key => $value)
    echo "Random object №$key (".spl_object_hash($value).") = ".$value->rand()."<br/>";

?>