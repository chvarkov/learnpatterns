<?php

require '../AppendIterator/Classes/Random10.php';

use AppendIterator\Random10;

//Test use AppendIterator

$ii = new InfiniteIterator(new ArrayIterator(
    [
        new Random10(), new Random10(), new Random10()
    ]
));

foreach ($ii as $key => $item)
{
    $rnd = $item->rand();
    echo "$key => $rnd <br/>";
    if ($rnd == 0) break;
}
?>