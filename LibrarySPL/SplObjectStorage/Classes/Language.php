<?php

namespace SplObjectStorage;

class Language
{
    private $name;

    public function __construct($name)
    {
        $this->name = $name;
    }

    public function __toString()
    {
        return "Language: ".$this->name;
    }


    public function getName()
    {
        echo $this->name;
    }
}

?>