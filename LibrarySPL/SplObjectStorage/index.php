<?php

require 'Classes/Language.php';

use SplObjectStorage\Language;

$os = new SplObjectStorage();
$php = new Language("php");
$os->attach($php);
$os->attach(new Language("js"));
$os->attach(new Language("c#"));
$os->attach(new Language("c++"));
$os->attach(new Language("python"));


echo ($os->contains($php))? "Found: ".$os[$php] : "Error: '".$php."' not found";

foreach ($os as $key => $value) {
    echo $value."<br/>";
}

?>