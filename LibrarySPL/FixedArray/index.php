<?php

$fruits = new SplFixedArray(4);

$fruits[0] = "Apple";
$fruits[1] = "Orange";
$fruits[2] = "Banana";
$fruits[3] = "Lemon";

$fruits->setSize(5);
$fruits[4] = "Lime";

foreach ($fruits as $value)
    echo "$value<br/>";

?>