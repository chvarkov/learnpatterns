<?php

require 'Classes/DefaultArrayAccess.php';

use ArrayAccess\DefaultArrayAccess;

//Test use ArrayAccess

//Test method offsetSet()
$class = new DefaultArrayAccess();
$class["car"] = "Audi";
$class["model"] = "A4";
$class["cost"] = 34566;
$class["color"] = "red";
$class["year"] = 2016;

//Test method offsetGet()
echo "Car: ".$class["car"]."\n";
echo "Model: ".$class["model"]."\n";

//Test method offsetIsset()
echo isset($class["cost"])? "Not free\n" : "Free!!!\n";

//Test method offsetUnset()
unset($class["cost"]);

//And yet check
echo isset($class["cost"])? "Not free\n" : "Free!!!\n";


?>