<?php

namespace ArrayAccess;


class DefaultArrayAccess implements \ArrayAccess
{
    private $properties;

    public function offsetExists($offset)
    {
        echo "\nCheck $offset in class\n";
        return isset($this->properties[$offset]);
    }

    public function offsetGet($offset)
    {
        echo "\nGet $offset from class\n";
        return $this->properties[$offset];
    }

    public function offsetSet($offset, $value)
    {
        echo "\nSet $value for $offset in class\n";
        $this->properties[$offset] = $value;
    }

    public function offsetUnset($offset)
    {
        echo "\nUnset $offset in class\n";
        unset($this->properties[$offset]);
    }
}

?>