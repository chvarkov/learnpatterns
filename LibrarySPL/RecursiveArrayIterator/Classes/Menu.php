<?php

namespace RecursiveArrayIterator;


use RecursiveIteratorIterator;
use Traversable;

class Menu extends \RecursiveIteratorIterator
{
    public function __construct(Traversable $iterator, int $mode = self::LEAVES_ONLY, int $flags = 0)
    {
        parent::__construct($iterator, $mode, $flags);
    }

    public function beginChildren()
    {
        echo "<ul>\n";
        parent::beginChildren();
    }

    public function endChildren()
    {
        parent::endChildren();
        echo "</ul>\n";
    }
}