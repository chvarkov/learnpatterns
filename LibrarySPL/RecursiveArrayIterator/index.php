<?php

require "Classes/Menu.php";

use RecursiveArrayIterator\Menu;

$menu = array(
    "Homepage",
    "Categories" => [
        "Dev",
        "Database",
        "Design"
    ],
    "About",
    "Contacts"

);

$r = new Menu( new RecursiveArrayIterator($menu),RecursiveIteratorIterator::SELF_FIRST);

echo "<ul>\n";
foreach ($r as $key => $value)
    echo (is_array($value))? "<li>$key</li>" : "<li>$value</li>\n";
echo "</ul>\n"

?>