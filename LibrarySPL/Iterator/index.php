<?php

require 'Classes/DefaultIterator.php';

use Iterator\DefaultIterator;

$array = ["Apple", "Lemon", "Mango"];

$list = new DefaultIterator($array);

foreach ($array as $value)
    echo "$value\n";

?>