<?php

namespace Iterator;

class SquareIterator implements \Iterator
{
    private $var = [];

    public function __construct($from, $to)
    {
        if ($from > 0 && $to >= $from)
            for ($i = $from; $i <= $to; $i++)
                $this->var[] = pow($i, 2);
        reset($this->var);
    }

    public function rewind() { }

    public function valid()
    {
        return $this->current() != null;
    }

    public function current()
    {
        return current($this->var);
    }

    public function key()
    {
        return key($this->var);
    }

    public function next()
    {
        next($this->var);
    }
}

?>