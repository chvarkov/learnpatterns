<?php

namespace Iterator;

class IncIterator implements \Iterator
{
    private $items = [];

    public function __construct($from, $to)
    {
        for($i = $from; $i <= $to; $i++)
            $this->items[] = $i + 1;
    }

    public function rewind() { }

    public function valid()
    {
        return current($this->items) != null;
    }

    public function current()
    {
        return current($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function next()
    {
        next($this->items);
    }

}

?>