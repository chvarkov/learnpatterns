<?php

namespace Iterator;

class DefaultIterator implements \Iterator
{
    private $items = [];

    public function __construct($array = null)
    {
        if ($array != null)
            $this->items = $array;
    }

    public function rewind()
    {
        reset($this->items);
    }

    public function valid()
    {
        return $this->current() !== null;
    }

    public function current()
    {
        return current($this->items);
    }

    public function key()
    {
        return key($this->items);
    }

    public function next()
    {
        next($this->items);
    }
}

?>