<?php

namespace FilterIterator;

use FilterIterator\Product;

class ProductFilterIterator extends \FilterIterator
{
    public function accept()
    {
        return $this->current()->getCost() >= 4;
    }
}