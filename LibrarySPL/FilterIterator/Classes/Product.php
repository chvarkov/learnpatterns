<?php

namespace FilterIterator;

class Product
{
    private $id, $title, $cost;

    public function __construct($id, $title, $cost)
    {
        $this->id = $id;
        $this->title = $title;
        $this->cost = $cost;
    }

    public function show()
    {
        echo "ID: $this->id; TITLE: $this->title; COST: $this->cost$<br/>\n";
    }

    public function getCost()
    {
        return $this->cost;
    }

}