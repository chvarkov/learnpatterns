<?php

require 'Classes/Product.php';
require 'Classes/ProductFilterIterator.php';

use FilterIterator\Product;
use FilterIterator\ProductFilterIterator;

$array = array(
    new Product(1, "Book name 1", 2), //Not in filter
    new Product(2, "Book name 2", 5),
    new Product(3, "Book name 3", 3), //Not in filter
    new Product(4, "Book name 4", 4),
    new Product(5, "Book name 5", 4.1),
    new Product(6, "Book name 6", 8)
);

$fi = new ProductFilterIterator(new ArrayIterator($array));

//Filter Product (Cost > 4)
foreach ($fi as $product)
    $product->show();

?>