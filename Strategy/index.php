<?php

require 'Interfaces/ConvertStrategy.php';
require 'Classes/Converter.php';
require 'Classes/Strategies/PHP.php';
require 'Classes/Strategies/AssocArray.php';
require 'Classes/Strategies/XML.php';
require 'Classes/Strategies/JSON.php';
require 'Classes/ForExample/User.php';

use Strategy\Converter;
use Strategy\ForExample\User;


//Test use Strategy

$exampleObject = new User(1, "admin", "admin@domain.com", "123123");
$exampleObject->addPost("This first post");
$exampleObject->addPost("This second ...");

$converterPHP = new Converter(Converter::PHP_VAR_EXPORT);
echo "Strategy PHP_VAR_EXPORT:\n";
print_r($converterPHP->convert($exampleObject));

$converterArray = new Converter(Converter::ARRAY);
echo "\n\nStrategy ARRAY:\n";
print_r($converterArray->convert($exampleObject));

$converterXML = new Converter(Converter::XML);
echo "\n\nStrategy XML:\n";
print_r($converterXML->convert($exampleObject));

$converterJSON = new Converter(Converter::JSON);
echo "\n\nStrategy JSON:\n";
print_r($converterJSON->convert($exampleObject));

?>