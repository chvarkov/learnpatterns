<?php

namespace Strategy;

interface ConvertStrategy
{
    public function convert($object);
}

?>