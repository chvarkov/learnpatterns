<?php

namespace Strategy;

use Strategy\Strategies\PHP;
use Strategy\Strategies\AssocArray;
use Strategy\Strategies\XML;
use Strategy\Strategies\JSON;

class Converter
{
    //Flags for strategy
    const PHP_VAR_EXPORT = 1;
    const ARRAY = 2;
    const XML = 3;
    const JSON = 4;

    private $strategyConvert;

    public function __construct($convertStrategyFlag)
    {
        switch ($convertStrategyFlag)
        {
            case 1: $this->strategyConvert = new PHP(); break;
            case 2: $this->strategyConvert = new AssocArray(); break;
            case 3: $this->strategyConvert = new XML(); break;
            case 4: $this->strategyConvert = new JSON(); break;
            default: throw new \Exception("This flag $convertStrategyFlag is not exist");
        }
    }

    public function convert($object)
    {
        return $this->strategyConvert->convert($object);
    }
}

?>