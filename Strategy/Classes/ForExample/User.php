<?php

namespace Strategy\ForExample;

//Create class User for convert him object
class User
{
    public $userId, $login, $email, $password, $posts;

    public function __construct($userId, $login, $email, $password)
    {
        $this->userId = $userId;
        $this->login = $login;
        $this->email = $email;
        $this->password = $password;
    }

    public function addPost($message)
    {
        $this->posts[] = ["message" => $message, "date" => date("H:i d.m.Y", time())];
    }
}

?>