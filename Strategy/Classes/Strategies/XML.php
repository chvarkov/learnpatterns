<?php

namespace Strategy\Strategies;

use Strategy\ConvertStrategy;

class XML implements ConvertStrategy
{
    //Recursive method for converted associative arrays to xml (SimpleXMLElement)
    private function toXML($array, $childXML = null)
    {
        $xml = ($childXML != null)? $childXML :  new \SimpleXMLElement("<Object/>");
        foreach ($array as $key => $value)
        {
            if (!is_array($value))
                $xml->addChild($key, $value);
            else
            {
                $childXML = $xml->addChild($key);
                $childXML = $this->toXML($value, $childXML);
            }
        }
        return $xml;
    }

    public function convert($object)
    {
        $array = (array)$object;
        $xml = $this->toXML($array);
        return $xml->asXML();
    }
}

?>