<?php

namespace Strategy\Strategies;

use Strategy\ConvertStrategy;

class AssocArray implements ConvertStrategy
{
    public function convert($object)
    {
        return (array)$object;
    }
}

?>