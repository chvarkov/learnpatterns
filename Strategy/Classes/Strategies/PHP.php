<?php

namespace Strategy\Strategies;

use Strategy\ConvertStrategy;

class PHP implements ConvertStrategy
{
    public  function convert($object)
    {
        return var_export($object);
    }
}

?>