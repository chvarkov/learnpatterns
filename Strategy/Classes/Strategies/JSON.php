<?php

namespace Strategy\Strategies;

use Strategy\ConvertStrategy;

class JSON implements ConvertStrategy
{
    public function convert($object)
    {
        return json_encode($object);
    }
}

?>