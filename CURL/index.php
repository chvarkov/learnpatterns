<?php

$curl = curl_init("http://localhost/pdo/get.php");
curl_setopt($curl, CURLOPT_RETURNTRANSFER, 1);
$result = curl_exec($curl);
$result = json_decode($result);
curl_close($curl);

?>

<!DOCTYPE html>
<html>
    <head>
        <title>All news</title>
    </head>
    <body>
        <table width="50%" align="center">
            <tr>
                <th colspan="3"><h1>All news</h1></th>
            </tr>
            <tr>
                <th><h2>Title</h2></th>
                <th><h2>Text</h2></th>
                <th><h2>Date</h2></th>
            </tr>
            <?php foreach ($result->response as $item): ?>
                <tr>
                    <th><?php echo $item->title ?></th>
                    <th><?php echo $item->text ?></th>
                    <th><?php echo $item->date ?></th>
                </tr>
            <?php endforeach; ?>
        </table>
    </body>
</html>
